# kamalak

A tiny mod to add support for [formatting codes][mcwiki]. Use `&` instead of
`§`. `&x` shows the item you're holding.

[*kamalak*][wiktionary] is Uzbek for 'rainbow'.

[mcwiki]: https://minecraft.gamepedia.com/Formatting_codes
[wiktionary]: https://en.wiktionary.org/wiki/kamalak#Uzbek
