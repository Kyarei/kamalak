package flirora.kamalak;

import net.minecraft.entity.player.PlayerEntity;
import net.minecraft.text.LiteralText;
import net.minecraft.text.Style;
import net.minecraft.text.Text;
import net.minecraft.util.Formatting;

public class KTextUtils {
  public static Text parseAmpString(String message, PlayerEntity player) {
    Text t = null;
    Style currentStyle = new Style();
    int i = 0;
    while (i < message.length()) {
      int ampIndex = message.indexOf('&', i);
      if (ampIndex == -1)
        ampIndex = message.length();
      String submessage = message.substring(i, ampIndex);
      if (t == null) {
        t = new LiteralText(submessage);
      } else {
        t.append(new LiteralText(submessage).setStyle(currentStyle));
      }
      if (ampIndex + 1 < message.length()) {
        i = ampIndex + 2;
        char letter = message.charAt(ampIndex + 1);
        switch (letter) {
        case '0':
          currentStyle = currentStyle.copy().setColor(Formatting.BLACK);
          break;
        case '1':
          currentStyle = currentStyle.copy().setColor(Formatting.DARK_BLUE);
          break;
        case '2':
          currentStyle = currentStyle.copy().setColor(Formatting.DARK_GREEN);
          break;
        case '3':
          currentStyle = currentStyle.copy().setColor(Formatting.DARK_AQUA);
          break;
        case '4':
          currentStyle = currentStyle.copy().setColor(Formatting.DARK_RED);
          break;
        case '5':
          currentStyle = currentStyle.copy().setColor(Formatting.DARK_PURPLE);
          break;
        case '6':
          currentStyle = currentStyle.copy().setColor(Formatting.GOLD);
          break;
        case '7':
          currentStyle = currentStyle.copy().setColor(Formatting.GRAY);
          break;
        case '8':
          currentStyle = currentStyle.copy().setColor(Formatting.DARK_GRAY);
          break;
        case '9':
          currentStyle = currentStyle.copy().setColor(Formatting.BLUE);
          break;
        case 'a':
        case 'A':
          currentStyle = currentStyle.copy().setColor(Formatting.GREEN);
          break;
        case 'b':
        case 'B':
          currentStyle = currentStyle.copy().setColor(Formatting.AQUA);
          break;
        case 'c':
        case 'C':
          currentStyle = currentStyle.copy().setColor(Formatting.RED);
          break;
        case 'd':
        case 'D':
          currentStyle = currentStyle.copy().setColor(Formatting.LIGHT_PURPLE);
          break;
        case 'e':
        case 'E':
          currentStyle = currentStyle.copy().setColor(Formatting.YELLOW);
          break;
        case 'f':
        case 'F':
          currentStyle = currentStyle.copy().setColor(Formatting.WHITE);
          break;
        case 'k':
        case 'K':
          currentStyle = currentStyle.copy().setColor(Formatting.OBFUSCATED);
          break;
        case 'l':
        case 'L':
          currentStyle = currentStyle.copy().setColor(Formatting.BOLD);
          break;
        case 'm':
        case 'M':
          currentStyle = currentStyle.copy().setColor(Formatting.STRIKETHROUGH);
          break;
        case 'n':
        case 'N':
          currentStyle = currentStyle.copy().setColor(Formatting.UNDERLINE);
          break;
        case 'o':
        case 'O':
          currentStyle = currentStyle.copy().setColor(Formatting.ITALIC);
          break;
        case 'r':
        case 'R':
          currentStyle = currentStyle.copy().setColor(Formatting.RESET);
          break;
        case 'x':
        case 'X':
          t.append(
              player.getStackInHand(player.getActiveHand()).toHoverableText());
          break;
        case '&':
          t.append("&");
          break;
        default:
          --i;
          t.append("&");
        }
      } else {
        i = ampIndex + 1;
        if (ampIndex < message.length())
          t.append("&");
      }
    }
    return t;
  }
}
