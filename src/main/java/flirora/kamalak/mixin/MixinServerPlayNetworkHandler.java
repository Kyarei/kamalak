package flirora.kamalak.mixin;

import org.spongepowered.asm.mixin.Mixin;
import org.spongepowered.asm.mixin.injection.At;
import org.spongepowered.asm.mixin.injection.Redirect;

import flirora.kamalak.KTextUtils;
import net.minecraft.server.network.ServerPlayNetworkHandler;
import net.minecraft.text.TranslatableText;

@Mixin(ServerPlayNetworkHandler.class)
public class MixinServerPlayNetworkHandler {
  @Redirect(
      method = "onChatMessage",
      at = @At(
          value = "NEW", target = "net/minecraft/text/TranslatableText",
          ordinal = 2
      )
  )
  public TranslatableText constructTranslatableText(String key, Object[] args) {
    assert key.equals("chat.type.text") && args[1] instanceof String;
    args[1] = KTextUtils.parseAmpString((String) args[1],
        ((ServerPlayNetworkHandler) (Object) this).player);
    return new TranslatableText(key, args);
  }
}
